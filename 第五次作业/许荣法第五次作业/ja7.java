import java.util.Scanner;

public class ja7 {
    public static void main(String[] args) {
        //7.	用户输入一个数字n，判断是否为素数(只能被1和自身整除的数)
        Scanner sc=new Scanner(System.in);
        System.out.print("请输入一个大于1的数：");
        int a= sc.nextInt(),b=2;
        boolean fold=true;
        while (b<=a/2){
            if (a%b==0) {
                fold=false;
                break;
            }
            b++;
        }
        if (fold){
            System.out.println(a+"是素数");
        }else{
            System.out.println(a+"不是素数");
        }
        System.out.println("程序结束！");
    }
}
