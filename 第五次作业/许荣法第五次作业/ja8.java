public class ja8 {
    public static void main(String[] args) {
        //8.	将1998-2008年之间的闰年年份输出
        int a=1998;
        System.out.print("1998-2008年之间的闰年有：");
        while (a<=2008){
            if (a%4==0&&a%100!=0||a%400==0) {
                System.out.println(a+";");
            }
            a++;
        }
        System.out.println("程序结束！");
    }
}
