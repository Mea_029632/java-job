//5.	判断用户输入的字母是不是元音字母(a o e i u v)
import java.util.Scanner;

public class n5 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("请输入一个字母：");
        String letter=sc.nextLine();
        if (letter.equals("a") || letter.equals("o") || letter.equals("e") || letter.equals("i") || letter.equals("u")){
            System.out.println(letter+"是元音字母");
        }else{
            System.out.println(letter+"不是元音字母");
        }
    }
}
