//4.	输入一个数，判断该数是不是3的倍数，并将结果输出
import java.util.Scanner;

public class n4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("请输入一个数");
        double count= sc.nextDouble();
        if (count%3==0){
            System.out.println(count+"是3的倍数");
        }else{
            System.out.println(count+"不是3的倍数");
        }
    }
}
