//7.	判断用户输入的年份是不是闰年，接受用户输入的年份后，首先要判断数字是不是正确的年份，
//        年份是四位数，闰年的条件是符合下面二者之一：①能被4整除，但不能被100整除；②能被400整除
import java.util.Scanner;

public class n7 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("请输入一个年份");
        int a= sc.nextInt();
        if (a>9999&&a<1000){
            if (a%4==0&&a%100!=0){
                System.out.println(a+"是闰年");
            }else if (a%400==0){
                System.out.println(a+"是闰年");
            }else {
                System.out.println(a + "不是闰年");
            }
        }
    }
}
