//6.	接受用户输入的两个数，分别求这两个数的和，差，积，商，模
import java.util.Scanner;

public class n6 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("请输入第一个数：");
        double a= sc.nextDouble();
        System.out.print("请输入第二个数：");
        double b= sc.nextDouble();
        System.out.println(a+"和"+b+"的和为："+(a+b));
        System.out.println(a+"和"+b+"的差为："+(a-b));
        System.out.println(a+"和"+b+"的积为："+(a*b));
        System.out.println(a+"和"+b+"的商为："+(a/b));
        System.out.println(a+"和"+b+"的模为："+(a%b));
    }
}
