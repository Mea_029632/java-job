//3.	用A,B,C等级对学生的的成绩分类, 其中0 - 59是A, 60 – 80是B,81 – 100 是C等级,
// 请用代码实现该功能,判断某位同学的分数等级,同学的的分数自己假设分数
import java.util.Scanner;

public class n3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("请输入一个分数：");
        double count= sc.nextDouble();
        if (count>100&&count<0){
            if (count<=59&&count>=0){
                System.out.println("该同学分数为：A级");
            }else if (count<=80&&count>=60){
                System.out.println("该同学分数为：B级");
            }else{
                System.out.println("该同学分数为：C级");
            }
        }
    }
}
